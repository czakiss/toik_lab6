package com.company;

import com.company.quiz.Quiz;
import com.company.quiz.QuizImpl;

public class Main {

    /**
     * metoda main powinna implementowac algorytm do
     * jak najszybszego wyszukiwania wartosci
     * zmiennej digit z klasy QuizImpl (zakladamy ze
     * programista nie zna zawartosci klasy QuizImpl).
     * Nalezy zalozyc, ze pole digit w klasie QuizImpl
     * moze w kazdej chwili ulec zmianie. Do wyszukiwania
     * odpowiedniej wartosci nalezy wykorzystywac tylko
     * i wylacznie metode isCorrectValue - jesli metoda
     * przestanie rzucac wyjatki wowczas mamy pewnosc ze
     * poszukiwana zmienna zostala odnaleziona.
     */
    public static void main(String[] args) {

        Quiz quiz = new QuizImpl();
        int digit = 0; //
        int left = Quiz.MIN_VALUE;
        int right =  Quiz.MAX_VALUE;


        for (int counter = 1; ; counter++) {

            try {

                digit = (left+right)/2;
                quiz.isCorrectValue(digit);
                System.out.println("Trafiona proba!!! Szukana liczba to: "
                        + digit + " Ilosc prob: " + counter);
                break;

            } catch (Quiz.ParamTooLarge paramTooLarge) {
                right = digit -1;
                System.out.println("Argument za duzy!!!");
                // implementacja logiki...
            } catch (Quiz.ParamTooSmall paramTooSmall) {
                left = digit + 1;
                System.out.println("Argument za maly!!!");
                // implementacja logiki...
            }
        }
    }
}

